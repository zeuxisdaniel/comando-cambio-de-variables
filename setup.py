from setuptools import setup

setup(
    name='cambioVariables-command',
    version='0.1',
    py_modules=['cambioVariables'],
    install_requires=[
        'Click', 'psycopg2'
    ],
    entry_points='''
        [console_scripts]
        cambioVariables=cambioVariables:cli
    ''',
)