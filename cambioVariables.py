#!/usr/bin/python
import sys
import psycopg2
import click

# Clases y variables globales
conn = None
separadorPropsConEspacios = "¬"
usuario_bd = "zaqzqp2d"
pwd_prev = "gptwdes6"
pwd_prod = "gptwpro6"
conexiones = {"mx" : {"laboratorio" : {"host" : "150.250.144.235", "database" : "bqzqpdmx", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prev},"cdr" : {"host" : "150.250.140.197", "database" : "bqzqpcdr", "puerto" : "5446", "user" : usuario_bd, "password" : pwd_prev},"desarrollo" : {"host" : "150.250.144.62", "database" : "bqzqpdmx", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prev},"test" : {"host" : "150.250.144.239", "database" : "bqzqptmx", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prev},"calidad" : {"host" : "150.250.144.241", "database" : "bqzqpcmx", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prev},"octa" : {"host" : "150.250.144.244", "database" : "bqzqpcmx", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prev},"liga-oculta" : {"host" : "150.100.38.90", "database" : "bqzqpomx", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prod},"produccion" : {"host" : "150.100.38.92", "database" : "bqzqppmx", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prod}, "brs" : {"host" : "150.214.134.222", "database" : "bqzqppmx", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prod}},"co" : {"desarrollo" : {"host" : "150.250.144.155", "database" : "bqzqpdco", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prev},"test" : {"host" : "150.250.144.156", "database" : "bqzqptco", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prev},"calidad" : {"host" : "150.250.144.158", "database" : "bqzqpcco", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prev},"liga-oculta" : {"host" : "150.250.134.162", "database" : "bqzqpoco", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prod},"produccion" : {"host" : "150.250.134.160", "database" : "bqzqppco", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prod}},"pe" : {"desarrollo" : {"host" : "150.250.144.74", "database" : "bqzqpdpe", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prev},"test" : {"host" : "150.250.144.76", "database" : "bqzqptpe", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prev},"calidad" : {"host" : "150.250.144.78", "database" : "bqzqpcpe", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prev},"liga-oculta" : {"host" : "150.250.134.164", "database" : "bqzqpope", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prod},"produccion" : {"host" : "150.250.134.59", "database" : "bqzqpppe", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prod}},"us" : {"desarrollo" : {"host" : "150.250.144.55", "database" : "bqzqpdus", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prev},"test" : {"host" : "150.250.144.217", "database" : "bqzqptus", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prev},"calidad" : {"host" : "150.250.144.199", "database" : "bqzqpcus", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prev},"liga-oculta" : {"host" : "150.250.134.201", "database" : "bqzqpous", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prev},"produccion" : {"host" : "150.250.134.203", "database" : "bqzqppus", "puerto" : "5447", "user" : usuario_bd, "password" : pwd_prod}}}

@click.group()
def cli():
	pass

def conectar(pais, entorno, variable_a_borrar, app_o_arq):
	if conexiones.get(pais, False):
		if conexiones[pais].get(entorno, False):
			try:
				# connect to the PostgreSQL server
				print('Conectando a la Base de Datos...')
				conn = psycopg2.connect(host=conexiones[pais][entorno]["host"], database=conexiones[pais][entorno]["database"], user=conexiones[pais][entorno]["user"], password=conexiones[pais][entorno]["password"], port=conexiones[pais][entorno]["puerto"])
				# create a cursor
				cursor = conn.cursor()
				like_query = "cfg." + variable_a_borrar + ".%"
				tamanio_substring = len(variable_a_borrar) + 6
				# Obtener versión maestra
				cursor.execute("SELECT cod_vermaest FROM qzqp.tqzqpmve WHERE xti_vermaact = 't'")
				versiones_maestras = cursor.fetchall()
				version_master = versiones_maestras[0][0]
				# Obtener valores aplicativos
				if app_o_arq == "app":
					cursor.execute("SELECT cod_aplicaci, SUBSTR(des_papnombr, %s) nombre, des_papvalor FROM qzqp.tqzqppap WHERE cod_papversi = %s AND des_papnombr LIKE %s", (int(tamanio_substring), version_master, like_query))
					props = cursor.fetchall()
				# Obtener valores arquitectura
				if app_o_arq == "arq":
					cursor.execute("SELECT qzqp.tqzqppcf.cod_gpropied, SUBSTR(qzqp.tqzqppcf.des_pcfnombr, %s), qzqp.tqzqppva.cod_uuaaosn, qzqp.tqzqppva.des_pvadescr FROM qzqp.tqzqppcf INNER JOIN qzqp.tqzqppva  ON qzqp.tqzqppcf.cod_propicfg = qzqp.tqzqppva.cod_propicfg WHERE qzqp.tqzqppcf.cod_pcfversi = %s AND qzqp.tqzqppcf.des_pcfnombr LIKE %s", (int(tamanio_substring), version_master, like_query))
					props = cursor.fetchall()

				cursor.close()
				return props
			except (Exception, psycopg2.DatabaseError) as error:
				print(error)
			finally:
				if conn is not None:
					conn.close()
					print('Se ha cerrado la conexión a la Base de Datos.')
		else:
			sys.exit("[Error] El entorno '" + entorno + "' no está permitido, los valores permitidos son: laboratorio, cdr, desarrollo, test, calidad, octa, liga-oculta y produccion.")
	else:
		sys.exit("[Error] El país '" + pais + "' no está permitido, los valores permitidos son: mx, pe, co y us.")

def crear_props_app(valores_app, variable_a_borrar, variable_a_poner, eliminar_variable):
	archivo_app = open('cambio-variables-app.properties', 'w')
	linea_a_imprimir = ""
	for propiedad in valores_app:
		uuaa = propiedad[0]
		prop_app = propiedad[1]
		valor_app = propiedad[2]
		valores_con_espacio = valor_app.split()
		if len(valores_con_espacio) > 1:
			valor_app = separadorPropsConEspacios + valor_app + separadorPropsConEspacios
		if uuaa == "enax":
			linea_a_imprimir = linea_a_imprimir + "cfg." + variable_a_poner + "." + prop_app + "\t" + valor_app + "\n"
		else:
			linea_a_imprimir = linea_a_imprimir + "cfg." + variable_a_poner + "." + prop_app + "\t" + "uuaa" + "\t" + uuaa + "\t" + valor_app + "\n"
		if eliminar_variable:
			linea_a_imprimir = linea_a_imprimir + "cfg." + variable_a_borrar + "." + prop_app + "\teliminar\ttodo\n"
	archivo_app.write(linea_a_imprimir)
	click.secho('Se ha generado correctamente el archivo cambio-variables-app.properties', fg='green')

def crear_props_arq(props_arq, variable_a_borrar, variable_a_poner, eliminar_variable):
	archivo_arq = open('cambio-variables-arq.properties', 'w')
	linea_a_imprimir = ""
	propiedades_no_repetidas = []
	for propiedad in props_arq:
		ambito = propiedad[0]
		nombre_prop = propiedad[1]
		uuaa_o_sn = propiedad[2]
		valor_prop = propiedad[3]
		linea_a_imprimir = linea_a_imprimir + "cfg." + variable_a_poner + "." + nombre_prop + "\t"
		valores_con_espacio = valor_prop.split()
		if len(valores_con_espacio) > 1:
			valor_prop = separadorPropsConEspacios + valor_prop + separadorPropsConEspacios
		if uuaa_o_sn != "qsrv":
			linea_a_imprimir = linea_a_imprimir + "sn\t" + uuaa_o_sn + "\t"
		linea_a_imprimir = linea_a_imprimir + "Logging\t" if ambito == "log" else linea_a_imprimir + "Otros\t"
		linea_a_imprimir = linea_a_imprimir + valor_prop + "\n"
		if eliminar_variable:
			if nombre_prop not in propiedades_no_repetidas:
				propiedades_no_repetidas.append(nombre_prop)
				linea_a_imprimir = linea_a_imprimir + "cfg." + variable_a_borrar + "." + nombre_prop + "\teliminar\ttodo\n"
	archivo_arq.write(linea_a_imprimir)
	click.secho('Se ha generado correctamente el archivo cambio-variables-arq.properties', fg='green')

#Comandos app y arq
@cli.command()
@click.argument('pais')
@click.argument('entorno')
@click.argument('variable_a_borrar')
@click.argument('variable_a_poner')
@click.option('--eliminar_variable', is_flag=True)
def app(pais, entorno, variable_a_borrar, variable_a_poner, eliminar_variable):
	"""
	Crea un archivo properties tomando propiedades aplicativas que comiencen con cfg.variable_a_borrar. y las cambia por cfg.variable_a_poner.

	\b
	- PAIS: País en el que se hará la consulta de datos, valores posibles: mx, pe, co y us.
	- ENTORNO: Entorno en el que se hará la consulta de datos, valores posibles: laboratorio, cdr, desarrollo, test, calidad, octa, liga-oculta y produccion.
	- VARAIABLE A BORRAR: Variable que será sustituida en las propiedades, ejemplos: lab, cdr, de, te, qa, octa, lo, pr, brs.
	- VARAIABLE A PONER: Variable que se sustituirá en las propiedades, ejemplos: ei, aus, oct.
	- ELIMINAR VARIABLE: Bandera opcional que indica la eliminación de las variables asociadas a la VARIABLE A BORRAR.
	"""
	crear_props_app(conectar(pais, entorno, variable_a_borrar, "app"), variable_a_borrar, variable_a_poner, eliminar_variable)

@cli.command()
@click.argument('pais')
@click.argument('entorno')
@click.argument('variable_a_borrar')
@click.argument('variable_a_poner')
@click.option('--eliminar_variable', is_flag=True)
def arq(pais, entorno, variable_a_borrar, variable_a_poner, eliminar_variable):
	"""
	Crea un archivo properties tomando propiedades de arquitectura que comiencen con cfg.variable_a_borrar. y las cambia por cfg.variable_a_poner.

	\b
	- PAIS: País en el que se hará la consulta de datos, valores posibles: mx, pe, co y us.
	- ENTORNO: Entorno en el que se hará la consulta de datos, valores posibles: laboratorio, cdr, desarrollo, test, calidad, octa, liga-oculta y produccion.
	- VARAIABLE A BORRAR: Variable que será sustituida en las propiedades, ejemplos: lab, cdr, de, te, qa, octa, lo, pr, brs.
	- VARAIABLE A PONER: Variable que se sustituirá en las propiedades, ejemplos: ei, aus, oct.
	- ELIMINAR VARIABLE: Bandera opcional que indica la eliminación de las variables asociadas a la VARIABLE A BORRAR.
	"""
	crear_props_arq(conectar(pais, entorno, variable_a_borrar, "arq"), variable_a_borrar, variable_a_poner, eliminar_variable)